# Устанавливаем базовый образ
FROM golang:1.21-alpine

# Указываем рабочую директорию внутри контейнера
WORKDIR /app

# Копируем go.mod и go.sum в рабочую директорию
COPY go.mod ./
COPY go.sum ./


# Загружаем зависимости
RUN go mod download

# Копируем все файлы из текущего каталога в рабочую директорию контейнера
COPY . .

# Копируем файл конфигурации
COPY config/config.yaml /app/config/config.yaml

# Собираем исполняемый файл
RUN go build -o app cmd/main/app.go

# Команда, которая будет исполнена при запуске контейнера из образа
CMD ["./app"]