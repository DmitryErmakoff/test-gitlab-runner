package config

// Структура конфига
type Config struct {
	Server   `yaml:"server"`
	Database `yaml:"database"`
}

// Структур сервера
type Server struct {
	Address string `yaml:"address"`
	Port    string `yaml:"port"`
}

// Структура базы данных
type Database struct {
	Host     string `yaml:"host"`
	Port     int    `yaml:"port"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	Dbname   string `yaml:"dbname"`
}
