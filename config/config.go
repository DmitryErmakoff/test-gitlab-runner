package config

import (
	"github.com/gookit/slog"
	"github.com/ilyakaznacheev/cleanenv"
	config "todo/config/model"
)

var Config = InitConfig()

func InitConfig() config.Config {
	var cfg config.Config
	err := cleanenv.ReadConfig("config/config.yaml", &cfg)
	if err != nil {
		slog.Error(err)
	}
	return cfg
}
