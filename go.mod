module todo

go 1.21.1

require (
	github.com/gookit/slog v0.5.5
	github.com/ilyakaznacheev/cleanenv v1.5.0
	github.com/julienschmidt/httprouter v1.3.0
	github.com/lib/pq v1.10.9
)

require (
	github.com/BurntSushi/toml v1.2.1 // indirect
	github.com/gookit/color v1.5.4 // indirect
	github.com/gookit/goutil v0.6.15 // indirect
	github.com/gookit/gsr v0.1.0 // indirect
	github.com/joho/godotenv v1.5.1 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/xo/terminfo v0.0.0-20220910002029-abceb7e1c41e // indirect
	github.com/yuin/goldmark v1.4.13 // indirect
	golang.org/x/mod v0.17.0 // indirect
	golang.org/x/sync v0.7.0 // indirect
	golang.org/x/sys v0.19.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	golang.org/x/tools v0.20.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	olympos.io/encoding/edn v0.0.0-20201019073823-d3554ca0b0a3 // indirect
)
