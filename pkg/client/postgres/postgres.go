package postgres

import (
	"database/sql"
	"fmt"
	"github.com/gookit/slog"
	_ "github.com/lib/pq"
	"todo/config"
)

var (
	host     = config.Config.Database.Host
	port     = config.Config.Database.Port
	user     = config.Config.Database.User
	password = config.Config.Database.Password
	dbname   = config.Config.Database.Dbname
)

var DB *sql.DB = ConnectDB()

func ConnectDB() *sql.DB {
	psqlconn := fmt.Sprintf("host=%s port=%v user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

	db, err := sql.Open("postgres", psqlconn)
	if err != nil {
		slog.Error(err)
		panic(err)
	}

	err = db.Ping()
	if err != nil {
		slog.Error(err)
		panic(err)
	}

	slog.Println("DB CONNECTED")
	return db
}
