package tasks

import (
	"encoding/json"
	"fmt"
	"github.com/julienschmidt/httprouter"
	"net/http"
	"strconv"
	"todo/internal/handlers"
	"todo/internal/users"
	"todo/pkg/client/postgres"
)

const (
	Tasks      = users.API + "/task"
	TaskID     = users.API + "/task/:id"
	Categories = users.API + "/categories"
	AddTask    = Tasks + "/add"
)

type handler struct {
}

func NewHandler() handlers.Handler {
	return &handler{}
}

func (h *handler) Register(router *httprouter.Router) {
	router.POST(Tasks, h.GetAllTasksByUserID)
	router.PUT(TaskID, h.UpdateTask)
	router.DELETE(TaskID, h.DeleteTask)
	router.GET(Categories, h.GetCategories)
	router.POST(AddTask, h.AddTask)
}

func (h *handler) GetAllTasksByUserID(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")

	var u users.User
	err := json.NewDecoder(r.Body).Decode(&u)
	if err != nil {
		w.WriteHeader(400)
		w.Write([]byte(err.Error()))
		return
	}
	fmt.Println(u.Id)
	query := fmt.Sprint(`SELECT t.id_task, t.task_name, t.description, t.status, c.category_name
FROM "Users" u
JOIN "UserTasks" ut ON u.id_user = ut.user_id
JOIN "Tasks" t ON ut.task_id = t.id_task
JOIN "Categories" c ON t.category_id = c.id_category
WHERE u.id_user = $1;
`)

	rows, err := postgres.DB.Query(query, u.Id)
	if err != nil {
		w.WriteHeader(400)
		w.Write([]byte(err.Error()))
		return
	}

	defer rows.Close()
	var task Task
	var taskArr []Task
	for rows.Next() {
		rows.Scan(&task.Id, &task.Name, &task.Description, &task.Status, &task.Category)
		taskArr = append(taskArr, task)
	}

	jsonResponse, err := json.Marshal(taskArr)
	w.WriteHeader(200)
	w.Write(jsonResponse)
}

func (h *handler) UpdateTask(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")

	var task Task

	taskStr := params.ByName("id")
	taskNmb, _ := strconv.Atoi(taskStr)
	json.NewDecoder(r.Body).Decode(&task)

	query := `
		UPDATE "Tasks"
		SET task_name = $1,
			description = $2,
			status = $3,
			category_id = $4
		WHERE id_task = $5;
	`

	_, err := postgres.DB.Exec(query, task.Name, task.Description, task.Status, task.CategoryID, taskNmb)
	if err != nil {
		w.WriteHeader(400)
		w.Write([]byte(err.Error()))
		return
	}

	w.WriteHeader(200)
	w.Write([]byte("Задача успешно изменена"))
}

func (h *handler) DeleteTask(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")

	taskStr := params.ByName("id")
	taskNmb, _ := strconv.Atoi(taskStr)

	query1 := `
		DELETE FROM "UserTasks"
		WHERE task_id = $1;
	`
	query2 := `
		DELETE FROM "Tasks"
		WHERE id_task = $1;`

	_, err := postgres.DB.Exec(query1, taskNmb)
	if err != nil {
		w.WriteHeader(400)
		w.Write([]byte(err.Error()))
		return
	}
	_, err = postgres.DB.Exec(query2, taskNmb)
	if err != nil {
		w.WriteHeader(400)
		w.Write([]byte(err.Error()))
		return
	}

	w.WriteHeader(200)
	w.Write([]byte("Задача была успешно удалена"))
}

func (h *handler) GetCategories(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")

	var task Task
	var taskArr []Task
	query := fmt.Sprintf(`SELECT * FROM "Categories"`)
	rows, err := postgres.DB.Query(query)
	if err != nil {
		w.WriteHeader(400)
		w.Write([]byte(err.Error()))
		return
	}
	defer rows.Close()
	for rows.Next() {
		rows.Scan(&task.CategoryID, &task.Category)
		taskArr = append(taskArr, task)
	}
	w.WriteHeader(200)
	jsonResponse, _ := json.Marshal(taskArr)
	w.Write(jsonResponse)
}

func (h *handler) AddTask(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")

	var task Task
	json.NewDecoder(r.Body).Decode(&task)
	fmt.Println(task)

	query := `
   WITH new_task AS (
      INSERT INTO "Tasks" ("task_name", "description", "status", "category_id")
      VALUES ($1, $2, $3, $4)
      RETURNING id_task
   )
   INSERT INTO "UserTasks" ("user_id", "task_id")
   SELECT $5, id_task
   FROM new_task;
`

	_, err := postgres.DB.Exec(query, task.Name, task.Description, task.Status, task.CategoryID, task.IdUser)

	if err != nil {
		w.WriteHeader(400)
		w.Write([]byte(err.Error()))
		return
	}
	w.WriteHeader(200)
	w.Write([]byte("Задача была успешно добавлена"))
}
