package tasks

type Task struct {
	Id          int    `json:"id_task,omitempty"`
	Name        string `json:"task_name,omitempty"`
	Description string `json:"description,omitempty"`
	Status      bool   `json:"status,omitempty"`
	CategoryID  int    `json:"id_category,omitempty"`
	Category    string `json:"category,omitempty"`
	IdUser      int    `json:"id_user,omitempty"`
}
