package users

func (u *User) RegisterUser() string {
	return `WITH new_email AS (
    INSERT INTO "Emails" ("email")
    VALUES ($1)
    RETURNING id_email
	)
	INSERT INTO "Users" ("username", "password", "email_id")
	SELECT $2, $3, id_email
	FROM new_email;`
}
