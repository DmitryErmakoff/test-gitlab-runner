package users

import (
	"encoding/json"
	"fmt"
	"github.com/gookit/slog"
	"github.com/julienschmidt/httprouter"
	"net/http"
	"todo/internal/handlers"
	"todo/pkg/client/postgres"
)

const (
	API         = "/api"
	UserService = API + "/user"
	RegisterURL = UserService + "/register"
	LoginURL    = UserService + "/login"
	LogoutURL   = UserService + "/logout"
)

type handler struct {
}

func NewHandler() handlers.Handler {
	return &handler{}
}

func (h *handler) Register(router *httprouter.Router) {
	router.POST(RegisterURL, h.RegisterUser)
	router.POST(LoginURL, h.LoginUser)
	router.POST(LogoutURL, h.LogoutUser)
}

func (h *handler) RegisterUser(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")

	var u User
	json.NewDecoder(r.Body).Decode(&u)

	_, err := postgres.DB.Exec(u.RegisterUser(), u.Email, u.Name, u.Password)
	if err != nil {
		w.WriteHeader(400)
		w.Write([]byte(err.Error()))
		return
	}
	w.WriteHeader(200)
	slog.Info(fmt.Sprintf("Пользователь %s, с почтой %s, успешно зарегистрирован"), u.Name, u.Email)
	w.Write([]byte("Пользователь успешно зарегистрирован"))
}

func (h *handler) LoginUser(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")

	var q Query

	var u User
	json.NewDecoder(r.Body).Decode(&u)
	q.Login()
	rows, err := postgres.DB.Query(q.QueryText, u.Name, u.Password)
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(u)
	defer rows.Close()
	for rows.Next() {
		var id int
		var username string
		var password string
		var email string
		rows.Scan(&id, &username, &password, &email)
		fmt.Println(id, username, password)
		fmt.Println("123")
		if u.Name == username && u.Password == u.Password {
			u.Id = id
			u.Name = username
			u.Password = password
			u.Email = email
			jsonResponse, _ := json.Marshal(u)
			w.WriteHeader(200)
			w.Write(jsonResponse)
			slog.Info(fmt.Sprintf("Пользователь %v вошел в систему"), u.Name)
			return
		}
	}
	w.WriteHeader(400)
	w.Write([]byte("Неправильное имя пользователя или пароль"))
	return
}

func (h *handler) LogoutUser(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")

	w.WriteHeader(200)
	w.Write([]byte("Выход выполнен"))
}
