package users

// User is a struct model containing
type User struct {
	Id       int    `json:"id_user,omitempty"`
	Name     string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
	Email    string `json:"email"`
}
