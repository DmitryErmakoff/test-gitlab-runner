package users

type Query struct {
	QueryText string
}

func (q *Query) Login() {
	q.QueryText = `SELECT u.id_user, u.username, u.password, em.email 
FROM "Users" u
JOIN "Emails" em ON u.id_user = em.id_email
WHERE u.username = $1 AND u.password = $2;`
}
