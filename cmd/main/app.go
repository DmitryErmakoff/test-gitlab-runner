package main

import (
	"github.com/gookit/slog"
	"github.com/julienschmidt/httprouter"
	"log"
	"net"
	"net/http"
	"time"
	"todo/config"
	"todo/internal/tasks"
	"todo/internal/users"
	"todo/pkg/client/postgres"
)

func main() {
	slog.Info("CREATE ROUTER")
	router := httprouter.New()
	handler := users.NewHandler()
	handler.Register(router)
	slog.Info("REGISTER USER HANDLER")
	handler = tasks.NewHandler()
	handler.Register(router)
	slog.Info("REGISTER TASKS HANDLER")
	err := postgres.DB.Ping()
	if err != nil {
		panic(err)
	}

	start(router)
}

func start(router *httprouter.Router) {
	slog.Info("START APPLICATION")

	listener, err := net.Listen("tcp", config.Config.Server.Port)
	if err != nil {
		slog.Error(err)
		panic(err)
	}

	server := &http.Server{
		Addr:         config.Config.Server.Address,
		Handler:      router,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	slog.Infof("SERVER IS LISTENING PORT %s%s", config.Config.Server.Address, config.Config.Server.Port)
	log.Fatalln(server.Serve(listener))
}
